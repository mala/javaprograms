package EmployeeExample;

public class Employee {
  private String empId;
  private String name;
  private double salary;

  public Employee(String empId, String name, double salary) {
    this.name = name;
    this.empId = empId;
    this.salary = salary;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmpId() {
    return empId;
  }

  public void setEmpId(String empId) {
    this.empId = empId;
  }

  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Employee{" +
        "empId='" + empId + '\'' +
        ", name='" + name + '\'' +
        ", salary=" + salary +
        '}';
  }
}
