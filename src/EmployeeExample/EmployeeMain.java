package EmployeeExample;

public class EmployeeMain {
  public static void main(String[] args) {
    Employee e1 = new Employee("B123","Athena",60300);
    Employee e2 = new Employee("B190","Arun",60300);
    Employee e3 = new Employee("B220","Alia",90300);

    compareSalary(e1,e2);
    compareSalary(e2,e3);
    compareSalary(e1,e3);
  }

  private static void compareSalary(Employee e1, Employee e2) {
    if (e1.getSalary() == e2.getSalary()) {
      System.out.println(e1.getName() + " have same salary as " + e2.getName());
    }
    else {
      System.out.println(e1.getName() + " have different salary as " + e2.getName());
    }
  }
}
