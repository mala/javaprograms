package EmployeeExample;

public class Manager extends Employee {

  private double bonus;
  public Manager(String empId, String name, double salary) {
    super(empId, name, salary);
  }

  @Override
  public String toString() {

    return "Manager{" +
        "bonus=" + bonus +
        '}';
  }
}
