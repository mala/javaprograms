import java.util.Scanner;

public class Factorial {


    public static int factorial(int input) {
        int product = 1;
        for (int i = 1; i <= input; i++) {
            product *=i;
        }
        return product;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        while (number != -1) {
            System.out.println(factorial(number) + " is factorial value of " + number);
            number = in.nextInt();
        }
    }
}
