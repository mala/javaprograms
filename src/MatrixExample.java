import java.util.Arrays;

public class MatrixExample {
  public static int[][] add(int[][] a, int[][] b) {
    if (a == null || b == null) {
      return null;
    }

    if (a.length != b.length) {
      return null;
    }

    if (a.length == 0 || (a[0].length != b[0].length)) {
      return null;
    }

    int numOfRows = a.length;
    int numOfColumns = a[0].length;
    int[][] sum = new int[numOfRows][numOfColumns];
    for (int i = 0; i < numOfRows; i++) {
      for (int j = 0; j < numOfColumns; j++) {
        sum[i][j] = a[i][j] + b[i][j];
      }
    }
    return sum;
  }

  public static int[][] multiply(int[][] a, int[][] b) {
    if (a == null || b == null) {
      return null;
    }

    if (a.length == 0 || a[0].length == 0 || b.length == 0 || b[0].length == 0) {
      return null;
    }

    int aRows = a.length, aCols = a[0].length;
    int bRows = b.length, bCols = b[0].length;
    if (aCols != bRows) {
      return null;
    }

    int[][] product = new int[aRows][bCols];
    for (int i = 0; i < aRows; i++) {
      for (int j = 0; j < bCols; j++) {
        for (int k = 0; k < aCols; k++) {
          product[i][j] += a[i][k] * b[k][j];
        }
      }
    }

    return product;
  }
}
