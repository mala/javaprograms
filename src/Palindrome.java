import java.util.Scanner;

public class Palindrome {

    public static boolean isPalindrome(String input) {

        char[] inputChar = input.toCharArray();
        int strLength = input.length();

        for (int i = 0; i < strLength / 2; i++) {
            if (inputChar[i] == inputChar[strLength - 1 - i]) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        while (!input.equals("0")) {
            System.out.println(input + " is " + (isPalindrome(input) ? "" : "not ") + "a palindrome.");
            input = in.nextLine();
        }
    }
}
