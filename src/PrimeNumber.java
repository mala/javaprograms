import java.util.Scanner;

public class PrimeNumber {
  private static boolean isPrime(int number) {
    if (number == 1) {
      return false;
    }

    if (number != 2 && number % 2 == 0) {
      return false;
    }

    int sqrt = (int) Math.pow(number, 0.5);
    for (int i = 3; i <= sqrt; i = i + 2) {
      if (number % i == 0) {
        return false;
      }
    }

    return true;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int number = in.nextInt();
    while (number != -1) {
      System.out.println(number + " is " + (isPrime(number) ? "": "not ") + "a prime number.");
      number = in.nextInt();
    }
  }
}
