package ShoppingSite;

import java.util.HashMap;
import java.util.Map;

public class Cart {
  private Map<Item, Integer> selectedItems;

  public Cart() {
    selectedItems = new HashMap<Item, Integer>();
  }

  public void addItem(Item item) {
    if (!selectedItems.containsKey(item)) {
      selectedItems.put(item, 0);
    }
    selectedItems.put(item, selectedItems.get(item) + 1);
  }

  public void removeItem(Item item) {
    if (selectedItems.containsKey(item)) {
      selectedItems.put(item, selectedItems.get(item) - 1);
      if (selectedItems.get(item) == 0) {
        selectedItems.remove(item);
      }
    }
  }

  public double getTotalPrice() {
    double cost = 0;
    for (Map.Entry<Item, Integer> entry: selectedItems.entrySet()) {
      cost += entry.getValue() * entry.getKey().getPrice();
    }
    return cost;
  }
}
