package ShoppingSite;

import java.util.*;
import java.util.stream.Collectors;

public class Catalog {
  private Set<Item> items;
  private Map<String, Set<String>> brandDetails;
  private Set<String> brands;
  private Set<String> categories;

  public Catalog() {
    brandDetails = new HashMap<>();
    categories = new HashSet<>();
    brands = new HashSet<>();
    items = new HashSet<>();
  }

  public void addNewBrand(String brandName, String itemType) {
    addCategory(itemType);
    addBrand(brandName);
    addBrandDetails(brandName, itemType);
  }

  private void addCategory(String itemType) {
    categories.add(itemType);
  }
  private void addBrand(String brandName) {
    brands.add(brandName);
  }

  private void addBrandDetails(String brandName, String itemType) {
    if (!brandDetails.containsKey(brandName)) {
      brandDetails.put(brandName, new LinkedHashSet<>());
    }
    brandDetails.get(brandName).add(itemType);
  }

  public void addItem(Item item) {
    final String brandName = item.getBrandName();
    items.add(item);
    addBrand(brandName);
    addCategory(item.getCategory());
    addBrandDetails(item.getBrandName(), item.getCategory());
  }

  public Set<String> getItemTypesForBrand(String branName) {
    return brandDetails.get(branName);
  }

  public Set<Item> getItemsForType(String itemType) {
    return items.stream()
        .filter(item -> Objects.equals(item.getCategory(), itemType))
        .collect(Collectors.toSet());
    /*
    Set<Item> filteredItems = new HashSet<>();
    for (Item item: items) {
      if (itemType != null && itemType.equals(item.getCategory())) {
        filteredItems.add(item);
      }
    }
    return filteredItems;
    */
  }

  public Set<Item> getItemsForBrand(String brandName) {
    return items.stream()
        .filter(item -> Objects.equals(item.getBrandName(), brandName))
        .collect(Collectors.toSet());
    /*
    Set<Item> filteredItems = new HashSet<>();
    for (Item item: items) {
      if (brandName != null && brandName.equals(item.getBrandName())) {
        filteredItems.add(item);
      }
    }
    return filteredItems;
    */
  }
}
