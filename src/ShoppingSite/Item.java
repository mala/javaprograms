package ShoppingSite;

import java.util.Objects;

public class Item {
  private int id;
  private String name;
  private String brandName;
  private String category;
  private double price;

  public Item(int id, String name, String brandName, String category, double price) {
    this.id = id;
    this.name = name;
    this.brandName = brandName;
    this.category = category;
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getBrandName() {
    return brandName;
  }

  public String getCategory() {
    return category;
  }

  public double getPrice() {
    return price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Item item = (Item) o;
    return id == item.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Item{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", brandName='" + brandName + '\'' +
        ", category='" + category + '\'' +
        ", price=" + price +
        '}';
  }
}
