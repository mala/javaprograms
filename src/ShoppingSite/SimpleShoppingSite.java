package ShoppingSite;

import ShoppingSite.Catalog;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SimpleShoppingSite {

  HashMap<String, HashSet> catlog;

  public static void main(String[] args) {
    Catalog catalog = new Catalog();
    catalog.addNewBrand("Puma", "Shoes");
    catalog.addNewBrand("Puma", "Shirts");
    catalog.addNewBrand("Levis", "Jeans");
    catalog.addNewBrand("Nike", "Shoes");
    catalog.addNewBrand("Nike", "Track Pants");
    catalog.addNewBrand("Nike", "Shorts");
    catalog.addNewBrand("Biba", "Kurti");
    catalog.addNewBrand("Biba", "Leggins");


    String bName = "Nike";
    HashSet<String> types = (HashSet<String>) catalog.getItemTypesForBrand(bName);
    if (types == null) {
      System.out.println("No items of Brand " + bName);
    } else {
      System.out.println("List of items of Brand " + bName);
      for (String name : types) {
        System.out.println("- " + name);
      }
    }

    Item item1 = new Item(1, "Blue Jeans", "Levis", "Jeans", 2900);
    Item item2 = new Item(2, "Printed Kurti", "Biba", "Kurti", 1900);
    Item item3 = new Item(3, "Light blue Leggin", "Biba", "Leggins", 650);
    Item item4 = new Item(4, "Black Jeans", "Levis", "Jeans", 3500);
    Item item5 = new Item(5, "Running shoes", "Nike", "Shoes", 5600);
    Item item6 = new Item(6, "Trekking shoes", "Nike", "Shoes", 9200);
    Item item7 = new Item(7, "Running shoes", "Puma", "Shoes", 3900);

    catalog.addItem(item1);
    catalog.addItem(item2);
    catalog.addItem(item3);
    catalog.addItem(item4);
    catalog.addItem(item5);
    catalog.addItem(item6);
    catalog.addItem(item7);

    String itemType = "Shoes";
    Set<Item> fItems = catalog.getItemsForType(itemType);
    if (fItems == null) {
      System.out.println("No items of Item type " + itemType);
    } else {
      System.out.println("List of items of Item Type " + itemType);
      for (Item item : fItems) {
        System.out.println("- " + item);
      }
    }
  }
}
