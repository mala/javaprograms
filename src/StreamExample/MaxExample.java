package StreamExample;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MaxExample {
  public static void main (String[] args) {
    List<Integer> list = Arrays.asList(2, 4, 1, 3, 7, 5, 9, 6, 8);

    Optional<Integer> maxNumber = list.stream().max(Integer::compareTo);
    System.out.println("max:"+maxNumber);

    Optional<Integer> minNumber = list.stream().min(Integer::compareTo);
    System.out.println("min:"+minNumber);

    maxNumber.ifPresent(System.out::println);
  }
}
