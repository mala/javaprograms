package StreamExample;


import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StringStream {



  public static void main(String[] args) {

    Predicate<String> stringLength = new Predicate<String>() {
      @Override
      public boolean test(String string) {
        return string.length() > 2;
      }
    };
    List<String> stringList = Arrays.asList("abc", "bcd", "defg", "jk");

    System.out.println( stringList.stream().filter(stringLength).collect(Collectors.toList()));
    System.out.println(stringList.stream().filter(s -> s.length() > 2).collect(Collectors.toList()));

    List<String> stringList1 = Arrays.asList("USA", "Japan", "France", "Germany", "India", "U.K.","Canada");
    System.out.println(stringList1.stream().map(s -> s.toUpperCase()).collect(Collectors.toList()));


    List<Integer> integerList = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
    System.out.println(integerList.stream().distinct().map(integer -> integer * integer).collect(Collectors.toList()));

    List<Integer> integerList1 = Arrays.asList(1,2,2,4,2,5);
    System.out.println(integerList1.stream().filter(integer ->  integer == 2).count());

  }

}
