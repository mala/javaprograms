package calculator;

import java.util.HashMap;
import java.util.Map;

public class Calculator {
  private Map<Character, Operator> operatorMap;

  public Calculator() {
    operatorMap = new HashMap<>();
    operatorMap.put('+', new Add());
    operatorMap.put('-', new Subtract());
    operatorMap.put('/', new Divide());
    operatorMap.put('*', new Multiply());
  }

  public static void main(String[] args) {

  }
}
