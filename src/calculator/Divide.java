package calculator;

public class Divide implements Operator {
  @Override
  public int apply(int a, int b) {
    if (b == 0) {
      throw new RuntimeException("Division by 0 is not defined");
    }
    return a / b;
  }

  @Override
  public double apply(double a, double b) {
    if (b == 0) {
      throw new RuntimeException("Division by 0 is not defined");
    }
    return a / b;
  }
}
