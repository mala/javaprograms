package calculator;

public class Multiply implements Operator {
  @Override
  public int apply(int a, int b) {
    return a * b;
  }

  @Override
  public double apply(double a, double b) {
    return a * b;
  }
}
