package calculator;

public interface Operator {
  int apply(int a, int b);
  double apply(double a, double b);
}
